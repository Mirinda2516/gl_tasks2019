#version 330

uniform sampler2D fogDiffuse;
uniform sampler2D diffuse;
uniform sampler2D depthTex;
uniform sampler2D depthFogMean;
uniform sampler2D depthFogMin;

in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const float coef = 0.05;

float zNear = 1.; 
float zFar  = 140.0; 
  
float LinearizeDepth(float depth) 
{
    // преобразуем обратно в NDC
    float z = depth * 2.0 - 1.0; 
    return (2.0 * zNear * zFar) / (zFar + zNear - z * (zFar - zNear));	
}

void main()
{
	vec3 fogColor = texture(fogDiffuse, texCoord).rgb;
	vec3 difColor = texture(diffuse, texCoord).rgb;
	float difDepth = LinearizeDepth(texture(depthTex, texCoord).r);
	float fogMaxDepth = LinearizeDepth(texture(depthFogMean, texCoord).r);
	float fogDepth = LinearizeDepth(texture(depthFogMin, texCoord).r);
	vec3 color = vec3(0.);

	if (fogDepth >= difDepth) {
		color = difColor;
	} else {
		float thicknes = min((fogMaxDepth - fogDepth), difDepth - fogDepth);
		float alpha = min(1., thicknes*coef);
		//float alpha = 0.75;
		color = fogColor * alpha + difColor * (1 - alpha);
		//color.r = alpha;
	}

	fragColor = vec4(color, 1.0); //просто копируем	
}
