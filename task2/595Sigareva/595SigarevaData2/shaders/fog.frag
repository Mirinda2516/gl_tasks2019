#version 330

uniform sampler2D fogTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
	float spot_cosinus;
	float K_c;
	float K_l;
	float K_q;
	bool is_directional;
};

uniform LightInfo lights[3];

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 diffuseCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;
float NdotP;

vec3 normal;

layout(location = 0) out vec4 colorTex; // "go to GL_COLOR_ATTACHMENT0"
//layout(location = 1) out vec3 maxDepth;

void curr_color(in LightInfo light, in vec3 diffuse_color, out vec3 color)
{
	vec3 lightDirCamSpace; //направление на источник света

	color = diffuse_color * light.La;

	if (light.is_directional){
		lightDirCamSpace = - posCamSpace.xyz;
		float LdotP = dot(light.pos, normalize(posCamSpace.xyz));
		if ( LdotP < light.spot_cosinus) {
			return;
		}
	} else {
		lightDirCamSpace = light.pos - posCamSpace.xyz;
	}

	float distance = length(lightDirCamSpace);
	lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света
	
	float NdotL = dot(normal, lightDirCamSpace.xyz); //скалярное произведение (косинус)

	if (NdotL * NdotP < 0.0){
		if (NdotL < 0) {
			NdotL *= -1;
		}
		float curr_coef = 1 / (light.K_c + light.K_l * distance + light.K_q * distance * distance);
		color += diffuse_color * light.Ld * curr_coef * NdotL;			
		vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну				
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		
		color += light.Ls * curr_coef * Ks * blinnTerm;
	}

}

void main()
{
	vec3 diffuseColor = texture(fogTex, diffuseCoord).rgb;

	colorTex = vec4(diffuseColor, 1.0f);
}
