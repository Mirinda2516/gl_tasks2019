set(SRC_FILES
    gl1_SimpleTriangle.cpp
	common/Application.cpp
	common/Camera.cpp
	common/DebugOutput.cpp
	common/Mesh.cpp
	common/ShaderProgram.cpp
	common/Texture.cpp
	common/Framebuffer.cpp
)

MAKE_OPENGL_TASK(595Sigareva 2 "${SRC_FILES}")

target_include_directories(595Sigareva2 PUBLIC
        common
)

if (UNIX)
    target_link_libraries(595Sigareva2 stdc++fs)
endif()
