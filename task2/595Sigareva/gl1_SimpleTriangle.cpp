#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <Framebuffer.hpp>

#include <iostream>
#include <sstream>
#include <vector>

/**
Пример z-fighting
*/
class SampleApplication : public Application
{
public:
	MeshPtr _plane;

	MeshPtr _marker; //Меш - маркер для источника света
	MeshPtr orbit_marker;

	//Идентификатор шейдерной программы
	ShaderProgramPtr _commonShader;
	ShaderProgramPtr _markerShader;

	//Переменные для управления положением одного источника света
	float _lr = 10.0f;
	float _phi = 2.65f;
	float _theta = 0.48f;

	float stat_lr = 10.0f;
	float stat_phi = 2.65f;
	float stat_theta = 0.48f;

	LightInfo _light;
	LightInfo orbit_light;
	LightInfo spot_light;

	TexturePtr _worldTex;
	TexturePtr _catTex;

	GLuint world_sampler;
	GLuint cat_sampler;

	float triag_tex_size1 = 0.;
	float triag_tex_size2 = 0.;

	std::string cats[10] = {
		"595SigarevaData2/cat4.png",
		"595SigarevaData2/cat2.png",
		"595SigarevaData2/cat3.png",
		"595SigarevaData2/cat9.png",
		"595SigarevaData2/cat10.png",
		"595SigarevaData2/cat6.png",
		"595SigarevaData2/cat7.png",
		"595SigarevaData2/cat8.png",
		"595SigarevaData2/cat.png",
		"595SigarevaData2/cat5.png"
	};

	TexturePtr cat_texs[10];

	int index = 0;


	ShaderProgramPtr _quadColorShader;
	ShaderProgramPtr _fogShader;

	GLuint _sampler;

	FramebufferPtr _gbufferFB;
	TexturePtr _depthTex;
	TexturePtr _normalsTex;
	TexturePtr _diffuseTex;

	int _oldWidth = 1024;
	int _oldHeight = 1024;

	MeshPtr _quad;
	MeshPtr _fogCube;

	TexturePtr _fogTex;

	FramebufferPtr _fogFB;
	TexturePtr _depthFogMinTex;
	TexturePtr _diffuseFogTex;

	FramebufferPtr _fogFB2;
	TexturePtr _depthFogMaxTex;

	CameraMoverPtr _orbitCameraMover;
	CameraMoverPtr _freeCameraMover;
	bool active_free_cam = true;

	SampleApplication() :
		_orbitCameraMover(std::make_shared<OrbitCameraMover>()),
		_freeCameraMover(std::make_shared<FreeCameraMover>())
	{
		_cameraMover = _freeCameraMover;
	}

	void initFramebuffers()
	{
		//Создаем фреймбуфер для рендеринга в G-буфер
		_gbufferFB = std::make_shared<Framebuffer>(1024, 1024);

		_normalsTex = _gbufferFB->addBuffer(GL_RGBA16F, GL_COLOR_ATTACHMENT0);
		_diffuseTex = _gbufferFB->addBuffer(GL_RGBA16F, GL_COLOR_ATTACHMENT1);
		_depthTex = _gbufferFB->addBuffer(GL_DEPTH_COMPONENT32F, GL_DEPTH_ATTACHMENT);

		_gbufferFB->initDrawBuffers();

		if (!_gbufferFB->valid())
		{
			std::cerr << "Failed to setup framebuffer\n";
			assert(false);
		}

		_fogFB = std::make_shared<Framebuffer>(1024, 1024);

		_diffuseFogTex = _fogFB->addBuffer(GL_RGB16F, GL_COLOR_ATTACHMENT0);
		_depthFogMinTex = _fogFB->addBuffer(GL_DEPTH_COMPONENT32F, GL_DEPTH_ATTACHMENT);

		_fogFB->initDrawBuffers();

		if (!_fogFB->valid())
		{
			std::cerr << "Failed to setup framebuffer\n";
			assert(false);
		}


		_fogFB2 = std::make_shared<Framebuffer>(1024, 1024);

		_fogFB2->addBuffer(GL_RGBA16F, GL_COLOR_ATTACHMENT0);
		_depthFogMaxTex = _fogFB2->addBuffer(GL_DEPTH_COMPONENT32F, GL_DEPTH_ATTACHMENT);

		_fogFB2->initDrawBuffers();

		if (!_fogFB2->valid())
		{
			std::cerr << "Failed to setup framebuffer\n";
			assert(false);
		}
	}

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Создание и загрузка мешей		

		_plane = make_surface(100., 300, triag_tex_size1, triag_tex_size2);
		_plane->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -3.0f)));

		_marker = makeSphere(0.1f);
		_marker->setModelMatrix(glm::translate(glm::mat4(1.0f),
			glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr));

		orbit_marker = makeSphere(0.1f);
		orbit_marker->setModelMatrix(glm::translate(glm::mat4(1.0f),
			glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr));

		_quad = makeScreenAlignedQuad();

		_fogCube = makeCube(5.f);
		_fogCube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -3.0f)));


		//=========================================================
		//Инициализация шейдеров

		_commonShader = std::make_shared<ShaderProgram>("595SigarevaData2/shaders/common.vert", "595SigarevaData2/shaders/common.frag");
		_markerShader = std::make_shared<ShaderProgram>("595SigarevaData2/shaders/marker.vert", "595SigarevaData2/shaders/marker.frag");
		_quadColorShader = std::make_shared<ShaderProgram>("595SigarevaData2/shaders/quadColor.vert", "595SigarevaData2/shaders/quadColor.frag");
		_fogShader = std::make_shared<ShaderProgram>("595SigarevaData2/shaders/fog.vert", "595SigarevaData2/shaders/fog.frag");

		//=========================================================
		//Инициализация значений переменных освщения
		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.2, 0.2, 0.2);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(1.0, 1.0, 1.0);
		_light.spot_cosinus = -1.;
		_light.attenuation0 = 1.0;
		_light.attenuation1 = 0.1;
		_light.attenuation2 = 0.02;
		_light.is_directional = false;

		orbit_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		orbit_light.ambient = glm::vec3(0., 0., 0.);
		orbit_light.diffuse = glm::vec3(0.7, 0.7, 0.7);
		orbit_light.specular = glm::vec3(1.0, 1.0, 1.0);
		orbit_light.spot_cosinus = -1.;
		orbit_light.attenuation0 = 1.0;
		orbit_light.attenuation1 = 0.1;
		orbit_light.attenuation2 = 0.02;
		orbit_light.is_directional = false;

		spot_light.ambient = glm::vec3(0., 0., 0.);
		spot_light.diffuse = glm::vec3(0.9, 0.9, 0.9);
		spot_light.specular = glm::vec3(0.0, 0.0, 0.0);
		spot_light.spot_cosinus = 0.6;
		spot_light.attenuation0 = 1.0;
		spot_light.attenuation1 = 0.02;
		spot_light.attenuation2 = 0.01;
		spot_light.is_directional = true;
		//=========================================================
		//Загрузка и создание текстур
		_worldTex = loadTexture("595SigarevaData2/images/earth_global.jpg");
		_fogTex = loadTexture("595SigarevaData2/fog.png");

		for (int i = 0; i < 10; ++i) {
			cat_texs[i] = loadTexture(cats[i]);
		}
		_catTex = cat_texs[0];

		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers(1, &world_sampler);
		glSamplerParameteri(world_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(world_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(world_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(world_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &cat_sampler);
		glSamplerParameteri(cat_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(cat_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(cat_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(cat_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		initFramebuffers();
	}

	float sum_dt = 0.;
	float move_min_dt = 0.05;
	float move_speed_1 = 0.;
	float move_speed_2 = -1.;
	float offset1 = 0.;
	float offset2 = 0.;

	void update() override {
		Application::update();
		if (active_free_cam) {
			_cameraMover = _freeCameraMover;
		}
		else {
			_cameraMover = _orbitCameraMover;
		}
		float speed = 0.1;
		_phi += speed * dt;
		if (_phi > 2.0f * glm::pi<float>()) {
			_phi -= 2.0f * glm::pi<float>();
		}
		orbit_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		orbit_marker->setModelMatrix(glm::translate(glm::mat4(1.0f), orbit_light.position));

		sum_dt += dt;

		if (sum_dt > move_min_dt) {
			int steps = sum_dt / move_min_dt;
			sum_dt -= steps * move_min_dt;
			offset1 += move_speed_1 * steps * triag_tex_size1;
			offset2 += move_speed_2 * steps * triag_tex_size2;
			if (fabs(offset2) > 1.) {
				offset2 = 0.;
				index = (++index) % 10;
				_catTex = cat_texs[index];
			}
		}

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);
		if (width != _oldWidth || height != _oldHeight)
		{
			_gbufferFB->resize(width, height);
			_fogFB->resize(width, height);
			_fogFB2->resize(width, height);
			_oldWidth = width;
			_oldHeight = height;
		}
	}

	void light_uniforms(const ShaderProgramPtr& shader, LightInfo light, std::string struct_name, glm::vec3 lightPosCamSpace)
	{
		shader->setVec3Uniform(struct_name + ".pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		shader->setVec3Uniform(struct_name + ".La", light.ambient);
		shader->setVec3Uniform(struct_name + ".Ld", light.diffuse);
		shader->setVec3Uniform(struct_name + ".Ls", light.specular);
		shader->setFloatUniform(struct_name + ".spot_cosinus", light.spot_cosinus);
		shader->setFloatUniform(struct_name + ".K_c", light.attenuation0);
		shader->setFloatUniform(struct_name + ".K_l", light.attenuation1);
		shader->setFloatUniform(struct_name + ".K_q", light.attenuation2);
		shader->setFloatUniform(struct_name + ".is_directional", light.is_directional);
	}

	void draw() override
	{
		//Рендерим геометрию сцены в G-буфер
		drawToGBuffer(_gbufferFB, _commonShader, _camera);
		drawToFogBuffer(_fogFB, _fogShader, _camera, false);
		glDepthFunc(GL_GREATER);
		drawToFogBuffer(_fogFB2, _fogShader, _camera, true);
		glDepthFunc(GL_LESS);
		drawToScreen(_quadColorShader, _diffuseFogTex);
	}

	void drawToFogBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera, bool clearDepth)
	{
		fb->bind();

		glViewport(0, 0, fb->width(), fb->height());
		if (clearDepth) {
			glClearDepth(0.0f);
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		

		shader->use();
		shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

		_light.position = glm::vec3(glm::cos(stat_phi) * glm::cos(stat_theta),
			glm::sin(stat_phi) * glm::cos(stat_theta), glm::sin(stat_theta)) * stat_lr;
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		light_uniforms(shader, _light, "lights[0]", lightPosCamSpace);

		orbit_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		orbit_marker->setModelMatrix(glm::translate(glm::mat4(1.0f), orbit_light.position));
		glm::vec3 orbitLightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(orbit_light.position, 1.0));
		light_uniforms(shader, orbit_light, "lights[1]", orbitLightPosCamSpace);

		light_uniforms(shader, spot_light, "lights[2]", glm::vec3(0., 0., -1.));

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		_fogTex->bind();
		shader->setIntUniform("fogTex", 0);


		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		{
			glm::mat4 modelMatrix = _fogCube->modelMatrix();

			shader->setMat4Uniform("modelMatrix", modelMatrix);
			shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * modelMatrix))));

			_fogCube->draw();
		}

		glUseProgram(0); //Отключаем шейдер

		fb->unbind(); //Отключаем фреймбуфер
		if (clearDepth) {
			glClearDepth(1.0f);
		}
	}

	void drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera)
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//====== РИСУЕМ ОСНОВНЫЕ ОБЪЕКТЫ СЦЕНЫ ======
		shader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_light.position = glm::vec3(glm::cos(stat_phi) * glm::cos(stat_theta),
			glm::sin(stat_phi) * glm::cos(stat_theta), glm::sin(stat_theta)) * stat_lr;
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		light_uniforms(shader, _light, "lights[0]", lightPosCamSpace);

		orbit_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		orbit_marker->setModelMatrix(glm::translate(glm::mat4(1.0f), orbit_light.position));
		glm::vec3 orbitLightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(orbit_light.position, 1.0));
		light_uniforms(shader, orbit_light, "lights[1]", orbitLightPosCamSpace);

		light_uniforms(shader, spot_light, "lights[2]", glm::vec3(0., 0., -1.));

		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0        
		glBindSampler(0, world_sampler);
		_worldTex->bind();
		shader->setIntUniform("worldTex", 0);

		glActiveTexture(GL_TEXTURE1);  //текстурный юнит 0        
		glBindSampler(1, cat_sampler);
		_catTex->bind();
		shader->setIntUniform("catTex", 1);

		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		{
			glm::mat4 modelMatrix = _plane->modelMatrix();

			shader->setFloatUniform("offset1", offset1);
			shader->setFloatUniform("offset2", offset2);
			shader->setMat4Uniform("modelMatrix", modelMatrix);
			shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * modelMatrix))));

			_plane->draw();
		}
		_catTex->unbind();

		//Рисуем маркеры для всех источников света		
		{
			_markerShader->use();

			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
			_markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
			_marker->draw();
		}

		{
			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), orbit_light.position));
			_markerShader->setVec4Uniform("color", glm::vec4(orbit_light.diffuse, 1.0f));
			orbit_marker->draw();
		}


		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler(0, 0);
		glBindSampler(1, 0);
		glUseProgram(0);
	}

	void drawToGBuffer(const FramebufferPtr& fb, const ShaderProgramPtr& shader, const CameraInfo& camera)
	{
		fb->bind();

		glViewport(0, 0, fb->width(), fb->height());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->use();
		shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
		shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

		drawScene(shader, camera);

		glUseProgram(0); //Отключаем шейдер

		fb->unbind(); //Отключаем фреймбуфер
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
			ImGui::Checkbox("Free camera", &active_free_cam);
			if (ImGui::CollapsingHeader("stat_light"))
			{
				ImGui::ColorEdit3("stat_ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("stat_diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("stat_specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("stat_radius", &stat_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("stat_phi", &stat_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("stat_theta", &stat_theta, 0.0f, glm::pi<float>());
			}
			if (ImGui::CollapsingHeader("orbit_light"))
			{
				ImGui::ColorEdit3("orbit_ambient", glm::value_ptr(orbit_light.ambient));
				ImGui::ColorEdit3("orbit_diffuse", glm::value_ptr(orbit_light.diffuse));
				ImGui::ColorEdit3("orbit_specular", glm::value_ptr(orbit_light.specular));

				ImGui::SliderFloat("orbit_radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("orbit_phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("orbit_theta", &_theta, 0.0f, glm::pi<float>());
			}
			if (ImGui::CollapsingHeader("spot_light"))
			{
				ImGui::ColorEdit3("spot_ambient", glm::value_ptr(spot_light.ambient));
				ImGui::ColorEdit3("spot_diffuse", glm::value_ptr(spot_light.diffuse));
				ImGui::ColorEdit3("spot_specular", glm::value_ptr(spot_light.specular));

			}
		}
		ImGui::End();
	}

	void drawToScreen(const ShaderProgramPtr& shader, const TexturePtr& inputTexture)
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader->use();

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		inputTexture->bind();
		shader->setIntUniform("fogDiffuse", 0);

		glActiveTexture(GL_TEXTURE1);
		glBindSampler(1, _sampler);
		_diffuseTex->bind();
		shader->setIntUniform("diffuse", 1);

		glActiveTexture(GL_TEXTURE2);
		glBindSampler(2, _sampler);
		_depthTex->bind();
		shader->setIntUniform("depthTex", 2);

		glActiveTexture(GL_TEXTURE3);
		glBindSampler(3, _sampler);
		_depthFogMaxTex->bind();
		shader->setIntUniform("depthFogMean", 3);

		glActiveTexture(GL_TEXTURE4);
		glBindSampler(4, _sampler);
		_depthFogMinTex->bind();
		shader->setIntUniform("depthFogMin", 4);

		glEnable(GL_FRAMEBUFFER_SRGB); //Включает гамма-коррекцию

		_quad->draw();

		glDisable(GL_FRAMEBUFFER_SRGB);

		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler(0, 0);
		glUseProgram(0);
	}

};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}