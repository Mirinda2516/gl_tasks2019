#include "cuda_soil.h"

#include <iostream>
#include <cuda_runtime_api.h>

#include <SOIL2.h>
#include <vector>

#include <common/CudaImage.h>

cudaError_t image_host_to_dev(const unsigned char* image, size_t size, int width, int height, int channels, std::shared_ptr<CudaImage4c> &d_image) {
	d_image = std::make_shared<CudaImage4c>(width, height, ResourceLocation::Device);
	return cudaMemcpy(d_image->getHandle(), image, width * height * 4 * sizeof(unsigned char), cudaMemcpyHostToDevice);
}

cudaError_t image_dev_to_host(std::shared_ptr<CudaImage4c> d_image, unsigned char* h_image) {
	return cudaMemcpy(h_image, d_image->getHandle(), d_image->getSize(), cudaMemcpyDeviceToHost);
}

std::shared_ptr<CudaImage4c> load_image_rgb(const char* path) {
	int width, height, channels;
	unsigned char* sourceImg = SOIL_load_image(path, &width, &height, &channels, 4);
	assert(channels == 4);
	std::cout << "load image from " << path << ": " << SOIL_last_result() << std::endl;

	std::shared_ptr<CudaImage4c> d_image;
	cudaError_t res = image_host_to_dev(sourceImg, (size_t)width * height * channels, width, height, channels, d_image);
	SOIL_free_image_data(sourceImg);
	checkCudaErrors(res);

	return d_image;
}

void save_image_rgb(const char* path, std::shared_ptr<CudaImage4c> d_image) {
	std::vector<unsigned char> h_image(d_image->getSize());
	checkCudaErrors(image_dev_to_host(d_image, h_image.data()));
	SOIL_save_image(path, SOIL_SAVE_TYPE_PNG, d_image->getWidth(), d_image->getHeight(), 4, h_image.data());
	std::cout << "save image to " << path << ": " << SOIL_last_result() << std::endl;
}
