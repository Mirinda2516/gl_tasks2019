#ifndef COMMON_FUNCTIONS_CUH
#define COMMON_FUNCTIONS_CUH

#include <glm/glm.hpp>

struct ImageWorkArea {
	glm::uvec2 offset; // Offset in image starting work area.
	glm::uvec2 workDim; // Size of work area.
	glm::uvec2 imageDim; // Size of image.

	ImageWorkArea(glm::uvec2 imageSize) : offset({0, 0}), workDim(imageSize), imageDim(imageSize) { }
	ImageWorkArea(glm::uvec2 _offset, glm::uvec2 _workDim, glm::uvec2 _imageDim) : offset(_offset), workDim(_workDim), imageDim(_imageDim) { }
};

#endif // COMMON_FUNCTIONS_CUH
